﻿using EmptyBox.Models;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EmptyBox
{
	public partial class EmptyBoxDetail : Form
	{
		public EmptyBoxDetail(EmptyBoxHModel emptyBoxHeader)
		{
			InitializeComponent();

			var repo = new Repository();

			List<EmptyBoxDModel> dataDetail = repo.getDataEmptyBoxDetail(emptyBoxHeader.EDN_NO);

			dataGridDetail.DataSource = dataDetail;

		}
	}
}
