﻿DECLARE @@sqlstate VARCHAR(MAX),
		@@EDN_NO VARCHAR(20)

--set @@EDN_NO = @EDN_NO
--set @@EDN_NO = ''

set 
@@sqlstate = 
	'SELECT 
		EDN_NO,
		EDN_DT,
		ROUTE_CD,
		CYCLE,
		TOTAL,
		CREATED_BY,
		CREATED_DT,
		CHANGED_BY,
		CHANGED_DT 
	FROM 
		TB_R_EMPTYBOX_H
	WHERE 
		EDN_NO = '''+@EDN_NO+''''


--IF(@EDN_NO != '')
--BEGIN
--	set @@sqlstate += ' AND EDN_NO = '''+ @EDN_NO + ''''
--END

execute(@@sqlstate)
