﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace EmptyBox.Models
{

	public class Repository
	{
		public string CON_STRING = "";
		public string root = Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\..\\SQL\\";

		public Repository()
		{
			this.CON_STRING = "EmptyBox.Properties.Settings.CON_STRING";
		}

		protected string sql(string file)
		{
			//string root = Global.app_path + "\\SQL\\";
			var path = Path.Combine(root, file + ".sql");
			Console.WriteLine(path);
			System.IO.StreamReader f = new System.IO.StreamReader(path);
			string query = f.ReadToEnd();
			return query;
		}

		public List<EmptyBoxHModel> getDataEmptyBoxHeader(string EDN_NO = "")
		{
			var res = new List<EmptyBoxHModel>();

			using (var db = new Database(CON_STRING))
			{
				db.CommandTimeout = 0;
				try
				{
					//res = db.Fetch<EmptyBoxHModel>(this.sql("getEmptyBoxHeader"), new { EDN_NO = EDN_NO });
					res = db.Fetch<EmptyBoxHModel>(@"
							SELECT 
								EDN_NO,
								EDN_DT,
								ROUTE_CD,
								CYCLE,
								TOTAL,
								CREATED_BY,
								CREATED_DT,
								CHANGED_BY,
								CHANGED_DT
							FROM
								TB_R_EMPTYBOX_H
							WHERE
								EDN_NO LIKE @EDN_NO",
					new { EDN_NO = $"%{EDN_NO}%" });
				}
				catch (Exception ex)
				{
					Console.WriteLine(string.Format("error getDataEmptyBoxHeader: {0}", ex.Message));
				}

				db.CloseSharedConnection();
			}
			return res;
		}

		public List<EmptyBoxDModel> getDataEmptyBoxDetail(string EDN_NO = "")
		{
			var res = new List<EmptyBoxDModel>();

			using (var db = new Database(CON_STRING))
			{
				db.CommandTimeout = 0;
				try
				{
					//res = db.Fetch<EmptyBoxDModel>(this.sql("getEmptyBoxHeader"), new { EDN_NO = EDN_NO });
					res = db.Fetch<EmptyBoxDModel>(@"
							SELECT 
								EDN_NO,
								UNIQUE_NO,
								PART_NO,
								QTY,
								CREATED_BY,
								CREATED_DT,
								CHANGED_BY,
								CHANGED_DT
							FROM
								TB_R_EMPTYBOX_D
							WHERE
								EDN_NO LIKE @EDN_NO",
					new { EDN_NO = $"%{EDN_NO}%" });
				}
				catch (Exception ex)
				{
					Console.WriteLine(string.Format("error getDataEmptyBoxDetail: {0}", ex.Message));
				}

				db.CloseSharedConnection();
			}
			return res;
		}

		public List<PosReaderModel> GetDataMPos(string IP_READER = "")
		{
			var res = new List<PosReaderModel>();

			using (var db = new Database(CON_STRING))
			{
				db.CommandTimeout = 0;
				try
				{
					//res = db.Fetch<PosReaderModel>(this.sql("getEmptyBoxHeader"), new { EDN_NO = EDN_NO });
					res = db.Fetch<PosReaderModel>(@"
							SELECT POS_CD
								  ,POS_NM
								  ,READER_IP
								  ,ANTENNA_ID
								  ,GROUP_POS
								  ,GAIN
								  ,SEQ  FROM TB_M_POS WHERE READER_IP = @IP_READER
								  ORDER BY ANTENNA_ID ASC",
					new { IP_READER = $"%{IP_READER}%" });
				}
				catch (Exception ex)
				{
					Console.WriteLine(string.Format("error GetDataMPos: {0}", ex.Message));
				}

				db.CloseSharedConnection();
			}
			return res;
		}
	}
}
