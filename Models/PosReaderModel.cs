﻿using PetaPoco;

namespace EmptyBox.Models
{
	public class PosReaderModel
	{
		[Column]
		public string POS_CD { get; set; }

		[Column]
		public string POS_NM { get; set; }

		[Column]
		public string READER_IP { get; set; }

		[Column]
		public int ANTENNA_ID { get; set; }

		[Column]
		public string GROUP_POS { get; set; }

		[Column]
		public int GAIN { get; set; }

		[Column]
		public int SEQ { get; set; }
	}
}
