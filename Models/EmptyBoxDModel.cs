﻿using PetaPoco;

namespace EmptyBox.Models
{
	[ExplicitColumns]
	[TableName("TB_R_EMPTYBOX_D")]
	[PrimaryKey("EDN_NO")]

	public class EmptyBoxDModel
	{
		[Column]
		public string EDN_NO { get; set; }
		[Column]
		public string UNIQUE_NO { get; set; }
		[Column]
		public string PART_NO { get; set; }
		[Column]
		public int QTY { get; set; }
		[Column]
		public string CREATED_BY { get; set; }
		[Column]
		public string CREATED_DT { get; set; }
		[Column]
		public string CHANGED_BY { get; set; }
		[Column]
		public string CHANGED_DT { get; set; }
	}
}
