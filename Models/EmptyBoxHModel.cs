﻿using PetaPoco;

namespace EmptyBox.Models
{
	[ExplicitColumns]
	[PrimaryKey("EDN_NO")]
	[TableName("TB_R_EMPTYBOX_H")]
	public class EmptyBoxHModel
	{
		[Column]
		public string EDN_NO { get; set; }
		[Column]
		public string EDN_DT { get; set; }
		[Column]
		public string ROUTE_CD { get; set; }
		[Column]
		public string ROUTE_SEQ { get; set; }
		[Column]
		public string CYCLE { get; set; }
		[Column]
		public int TOTAL { get; set; }
		//[Column]
		//public int TL_ID { get; set; }
		//[Column]
		//public int OPR_ID { get; set; }
		[Column]
		public string CREATED_BY { get; set; }
		[Column]
		public string CREATED_DT { get; set; }
		[Column]
		public string CHANGED_BY { get; set; }
		//[Column]
		//public string CHANGED_DT { get; set; }
		//[Column]
		//public string COMPANY_CD { get; set; }
		//[Column]
		//public string COMPANY_ID { get; set; }

		//public List<EmptyBoxDModel> Details { get; set; }

		//[ResultColumn]
		//public string StartDate { get; set; }

		//[ResultColumn]
		//public string EndDate { get; set; }
	}
}
