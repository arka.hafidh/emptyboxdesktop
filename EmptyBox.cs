﻿using EmptyBox.Models;
using System.Windows.Forms;

namespace EmptyBox
{
	public partial class EmptyBox : Form
	{
		public EmptyBox()
		{
			InitializeComponent();
		}

		private void searchBtn_Click(object sender, System.EventArgs e)
		{
			var repo = new Repository();


			var textSearch = searchText.Text;

			var result = repo.getDataEmptyBoxHeader(textSearch);

			dataGrid.DataSource = result;

			DataGridViewButtonColumn detailView = new DataGridViewButtonColumn();
			detailView.Name = "detail";
			detailView.Text = "View Detail";
			detailView.HeaderText = "DETAIL";
			detailView.UseColumnTextForButtonValue = true;

			int columnIndex = dataGrid.Columns.Count;

			if (dataGrid.Columns["detail"] == null)
			{
				dataGrid.Columns.Insert(columnIndex, detailView);
			}
		}

		private void dataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex == dataGrid.Columns["detail"].Index)
			{
				var emptyBoxHeader = (EmptyBoxHModel)dataGrid.CurrentRow.DataBoundItem;
				var emptyBoxDetail = new EmptyBoxDetail(emptyBoxHeader);


				emptyBoxDetail.Show();
			}
		}
	}
}
