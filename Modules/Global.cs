﻿using System.IO;
using System.Reflection;

namespace EmptyBox.Modules
{
	public static class Global
	{
		public static string app_path = ((Properties.Settings.Default.path == "") ?
			Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location.ToString()) : (Properties.Settings.Default.path));
		public static string assembly_name = Assembly.GetExecutingAssembly().GetName().Name;
	}
}
